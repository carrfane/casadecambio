class ChangeDataTypeForCambio2 < ActiveRecord::Migration[5.1]
  def change
  	change_column :cambios, :cuenta, :string
  	change_column :cambios, :cedula, :string
  end
end
