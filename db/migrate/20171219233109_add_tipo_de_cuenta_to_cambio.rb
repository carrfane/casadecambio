class AddTipoDeCuentaToCambio < ActiveRecord::Migration[5.1]
  def change
    add_column :cambios, :tipo_de_cuenta, :string
  end
end
