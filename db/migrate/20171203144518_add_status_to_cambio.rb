class AddStatusToCambio < ActiveRecord::Migration[5.1]
  def change
    add_column :cambios, :status, :integer, default: 0
  end
end
