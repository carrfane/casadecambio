class CreateComentarios < ActiveRecord::Migration[5.1]
  def change
    create_table :comentarios do |t|
      t.string "comment"
      t.integer "user_id"
      t.integer "cambio_id"
      t.timestamps
    end
  end
end
