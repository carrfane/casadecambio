class AddNameToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :nombre, :text
    add_column :accounts, :banco, :text
    add_column :accounts, :cuenta, :text
    add_column :accounts, :cedula, :text
    add_column :accounts, :email, :text
    add_column :accounts, :tipo_de_cuenta, :text
  end
end