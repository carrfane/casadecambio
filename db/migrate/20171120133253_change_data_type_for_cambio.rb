class ChangeDataTypeForCambio < ActiveRecord::Migration[5.1]
  def change
  	change_column :cambios, :bolivares, :float
  	change_column :cambios, :pesos, :float
  	change_column :cambios, :taza, :float
  	change_column :cambios, :cuenta, :float
  	change_column :cambios, :cedula, :float
  end
end
