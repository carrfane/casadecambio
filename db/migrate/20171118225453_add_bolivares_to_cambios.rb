class AddBolivaresToCambios < ActiveRecord::Migration[5.1]
  def change
    add_column :cambios, :bolivares, :decimal
    add_column :cambios, :pesos, :integer
    add_column :cambios, :taza, :decimal
    add_column :cambios, :banco, :string
    add_column :cambios, :cuenta, :integer
    add_column :cambios, :cedula, :integer
    add_column :cambios, :email, :string
  end
end
