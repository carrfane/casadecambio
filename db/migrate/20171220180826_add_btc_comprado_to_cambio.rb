class AddBtcCompradoToCambio < ActiveRecord::Migration[5.1]
  def change
    add_column :cambios, :btc_comprado, :float
    add_column :cambios, :btc_vendido, :float
  end
end
