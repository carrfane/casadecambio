class CambiosController < ApplicationController
	skip_before_action :verify_authenticity_token
	
	require 'khipu-api-client'
    before_action :find_user, only: [:index, :new, :create, :edit]
    before_action :find_cambio, only: [:cancelar, :procesada, :edit, :update]
    before_action :set_locale
    before_action :correct_user, only: [:edit]
	def index
		if find_user
			@cambios = find_user.admin ? Cambio.where(status: "pendiente").paginate(:page => params[:page]).order("id DESC") : Cambio.where(user_id: find_user.id).paginate(:page => params[:page]).order("id DESC")
		else
			flash[:alert] = I18n.t :need_login
			redirect_to new_user_session_path
		end
	end

	def new
		@rate = get_rate
		if find_user == nil
			@cambio = Cambio.new
		else
			@cambio = @user.cambios.new
		end
	end

	def create
		if  params["account"].first.empty? != true
			@account_id = params["account"].first
			@account = Account.find(@account_id)
			@cambio = @user.cambios.build(nombre: @account.nombre, banco: @account.banco, cuenta: @account.cuenta, cedula: @account.cedula, email: @account.email, tipo_de_cuenta: @account.tipo_de_cuenta, bolivares: params[:cambio][:bolivares], pesos: params[:cambio][:pesos], taza: params[:cambio][:taza])
		else
		    @cambio = @user.cambios.build(cambio_params)
		end
		if @cambio.save
			CambioMailer.cambio_mailer(@cambio).deliver_now
			khipu_config
			khipu_payment(@cambio,@user)
			#CambioMailer.cambio_mailer(@cambio).deliver_now
		else
			render "new"
		end
	end

	def edit
	end

	def update
		@cambio.update(cambio_params)
		redirect_to cambios_path
	end

	def show
		@cambio = Cambio.find(params[:id])
	end

	def cancelar
		@cambio.destroy
		flash[:notice] = "Su cambio no pudo ser procesado"
		redirect_to new_cambio_path
	end

	def procesada
		flash[:notice] = "Solicitud Creada"
		redirect_to new_cambio_path
	end

	private

	def get_rate
		GetRateService.new.rate_calculation
	end

	def cambio_params
		params.require(:cambio).permit!
	end

	def find_user
		@user ||= current_user
	end

	def set_locale
		I18n.locale = params[:locale] || I18n.default_locale
	end

	def khipu_config
		receiver_id = 154596
		secret_key = '619e46972f700e28aee2c0c73bed6576750ba56a'
		Khipu.configure do |c|
		  c.secret = secret_key
		  c.receiver_id = receiver_id
		  c.platform = 'mi casa de acmbio'
		  c.platform_version = '2.0'
		  # c.debugging = true
		end
	end

	def find_cambio
		@cambio = Cambio.find(params[:id])
	end

	def khipu_payment(cambio, usuario)
		@motivo = "Envio de Bsf #{cambio.bolivares.floor}"
		@transaction_id = "#{cambio.id}"
		@valor = cambio.pesos
		@usuario = usuario.email
		client = Khipu::PaymentsApi.new
		response = client.payments_post(@motivo, 'CLP', @valor, {
			transaction_id: "#{@transaction_id}",
			picture_url: "http://fotos.e-consulta.com/remesas_3_0_0_0.jpg",
			expires_date: DateTime.now + 0.0104,
		    payer_email: "#{@usuario}",
		    send_email: false,
		    send_reminders: false,
		    return_url: root_url+"/cambios/#{@transaction_id}/procesada",
		    cancel_url: root_url+"/cambios/#{@transaction_id}/cancelar"
		})
		redirect_to response.transfer_url
	end

	def correct_user
      unless @user.admin
      	flash[:notice] = "Usuario debe ser administrador"
      	redirect_to cambios_path
      end
	end
end