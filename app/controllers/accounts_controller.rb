class AccountsController < ApplicationController
  before_action :find_user, only: [:index, :new, :create]
  #skip_before_action :authenticate_user!
  
  def index
    @accounts = Account.where(user_id: params[:user_id])
    respond_to do |format|
      format.html
      format.json { render :json => @accounts }
    end

  end
  
  def new
    @account = @user.accounts.new
  end

  def create
    @account = @user.accounts.build(accounts_params)
    if @account.save
      flash[:notice] = "Cuenta de beneficiario guardada"
      redirect_to root_path 
    end
  end

  def show
    @account = Account.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render :json => @account }
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def find_user
    @user ||= current_user
    if @user == nil
      redirect_to new_user_session_path
    end
  end

  def accounts_params
    params.require(:account).permit!
  end
end
