class CambioMailer < ApplicationMailer
  
  def cambio_mailer(cambio)
    @cambio = cambio
    mail(to:["carrfane@gmail.com", @cambio.email, "yutingchien.tw@gmail.com"], subject:"Nueva Solicitud de Cambio")
  end

end
