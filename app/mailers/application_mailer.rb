class ApplicationMailer < ActionMailer::Base
  default from: 'carrfane@gmail.com'
  layout 'mailer'
end
