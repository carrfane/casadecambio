// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require jquery
//= require jquery_ujs
//= require bootstrap
var ready = function(){
	var tazaBolivaresDolar,
		tazaPesoDolar,
		tazaReal,
		tazaDeCambio = $('#rate').text()
	$('#cambio_bolivares').keyup(function(){
		var bolivares = $(this).val();
		$('#cambio_pesos').val((bolivares/tazaDeCambio).toFixed(2))
	})

	$('#cambio_pesos').keyup(function(){
		var bolivares = $(this).val();
		$('#cambio_bolivares').val((bolivares*tazaDeCambio))
		if ($(this).val() < 20000) {
			$('#alerta').text('Monto minimo a transferir $20.000')
		}
		else {
			$('#alerta').empty()
		}
	})

	$('#cambio_taza').val(tazaDeCambio);
	$('title').append(' Taza actual 1 CLP = '+tazaDeCambio+' BSF');

  $("#account_").on("change", function(){
  	var user = $("input#hidden").val(),
  	    account = $("#account_").val();
  	$.ajax({
      url: "/users/"+user+"/accounts/"+account+".json",
      method: "get",
      success: function(data){
        $("#cambio_nombre").val(data["nombre"])
        $("#cambio_banco").val(data["banco"])
        $("#cambio_cedula").val(data["cedula"])
        $("#cambio_tipo_de_cuenta").val(data["tipo_de_cuenta"])
        $("#cambio_cuenta").val(data["cuenta"])
        $("#cambio_email").val(data["email"])
      }
    })
  })
}

$(document).ready(ready)
$(document).on('turbolinks:load', ready)