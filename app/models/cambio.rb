class Cambio < ApplicationRecord
	enum status: [ :pendiente, :aceptada, :ejecutada ]
	belongs_to :user, optional: true
	has_many :comentarios

	def self.per_page
		10
	end
end