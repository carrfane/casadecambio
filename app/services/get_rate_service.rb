class GetRateService

	def initialize
		url_dolar_today = 'https://s3.amazonaws.com/dolartoday/data.json'
		uri_dolar_today = URI(url_dolar_today)
		response_dolar_today = Net::HTTP.get(uri_dolar_today)
		dolar_today = JSON.parse(response_dolar_today)["USD"]["transfer_cucuta"]
		# datos del peso_dolar
		url_peso_dolar = 'https://mindicador.cl/api' #'http://api.sbif.cl/api-sbifv3/recursos_api/dolar?apikey=b6086bc8bdc356a7643d6d2787cb337159976e67&formato=json'
		uri_peso_dolar = URI(url_peso_dolar)
		response_peso_dolar = Net::HTTP.get(uri_peso_dolar)
		peso_dolar = JSON.parse(response_peso_dolar)["dolar"]["valor"].to_f
		@dolar_today = dolar_today
		@peso_dolar = peso_dolar
	end

	def rate_calculation
		(@dolar_today/@peso_dolar)*0.805
	end
end