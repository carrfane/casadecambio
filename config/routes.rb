Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  root to: 'cambios#new'

  resources :cambios do
  	member do
  		get 'cancelar'
  		get 'procesada'
  	end
  end
  resources :users, only: [:show] do
    resources :accounts
  end
end
