# README

Proyecto Casa de Cambio, Surgio de la necesidad de crear una plataforma para el envio de remesas a venezuela desde santiago de Chile.

1. Clonar repositorio
2. corre bundle install para instalar las gemas necesarias.
3. db:schema:load para la creacion de las tablas necesarias.
4. en la carpeta services reside el script que se comunica con la API de sbif para la obtencion del precio del dolar en tiempo real.
5. en cambios_controller.rb reside la configuracion para utilizar la api de khipu como forma de pago (en el futuro sera movida a servicios)